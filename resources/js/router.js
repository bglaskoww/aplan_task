window.Vue = require('vue');
import VueRouter from 'vue-router'
window.Vue.use(VueRouter)

import ViewComponent from "./components/ViewComponent";

var routes = [
  {
    path: '/', component: ViewComponent,
  },
];

const router = new VueRouter({
  routes
})
export {router}
