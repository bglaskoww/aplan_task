window.Vue = require('vue');
import VueRouter from 'vue-router'
window.Vue.use(VueRouter)

import HomeComponent from "../components/AppComponent.vue";

var routes = [
  {
    path: '/', component: HomeComponent,
  },

];

const router = new VueRouter({
  routes
})
export {router}
