<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $number = rand(0,50);
        return [
            'name' => $this->faker->name,
            'parent_id' => Category::find($number) ? Category::find($number) : Category::factory()->create(),
            'weight' => rand(0,4),
        ];
    }
}
