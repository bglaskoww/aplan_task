<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $number = rand(0,50);
        return [
            'name' => $this->faker->name,
            'cat_id' => Category::find($number) ? Category::find($number) : Category::factory()->create(),
            'visible' => $this->faker->boolean,

        ];
    }
}
