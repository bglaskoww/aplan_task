<?php

namespace App\Http\Controllers;

use App\Services\CategoryProductsInterface;

class CategoryProductsController extends Controller
{
    private $categoryProducts;

    public function __construct(CategoryProductsInterface $categoryProducts)
    {
        $this->categoryProducts = $categoryProducts;
    }

    public function show($categoryId)
    {
        try {
            if ($categoryId) {
                return $this->categoryProducts->getAllProductsForCategory($categoryId);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return 'show method';
    }
}
