<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Services\BuildTreeServiceInterface;
use App\Services\CategoryProductsInterface;

class CategoryController extends Controller
{
    private $buildTreeService;
    /**
     * @var CategoryProductsInterface
     */
    public function __construct(BuildTreeServiceInterface $buildTreeService)
    {
        $this->buildTreeService = $buildTreeService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $all_categories = Category::withCount('products')->get()->toArray();

        return $this->buildTreeService->buildTree($all_categories, 0);
    }
}

