<?php

namespace App\Providers;

use App\Services\BuildTreeService;
use App\Services\BuildTreeServiceInterface;
use Illuminate\Support\ServiceProvider;

class BuildTreeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( BuildTreeServiceInterface::class,BuildTreeService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
