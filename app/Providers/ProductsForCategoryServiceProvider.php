<?php

namespace App\Providers;

use App\Services\CategoryProductsInterface;
use App\Services\CategoryProductsService;
use Illuminate\Support\ServiceProvider;

class ProductsForCategoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind( CategoryProductsInterface::class,CategoryProductsService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
