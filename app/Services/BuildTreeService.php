<?php


namespace App\Services;

class BuildTreeService implements BuildTreeServiceInterface
{
    public function buildTree(array &$elements, $parentId = 0, &$categoryIds = [])
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $categoryIds[] = $element['id'];

                $children = $this->buildTree($elements, $element['id'], $categoryIds);
                $tmp = [
                    'id' => $element['id'],
                    'name' => $element['name'],
                    'products_count' => $element['products_count'],
                    'total_products' => $element['products_count'],
                ];

                if ($children) {
                    $tmp['children'] = $children;

                    foreach ($children as $child) {
                        $tmp['total_products'] += $child['total_products'];
                    }
                }
                $branch[] = $tmp;
            }
        }
        return $branch;
    }
}