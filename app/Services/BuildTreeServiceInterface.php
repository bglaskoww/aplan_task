<?php


namespace App\Services;

interface BuildTreeServiceInterface
{
    public function buildTree(array &$elements, $parentId = 0, &$categoryIds = []);
}