<?php


namespace App\Services;

interface CategoryProductsInterface
{
    public function getAllProductsForCategory($categoryId);
}