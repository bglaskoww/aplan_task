<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Product;

/**
 * @property BuildTreeService buildTreeService
 */
class CategoryProductsService implements CategoryProductsInterface
{
    public function __construct(BuildTreeService $buildTreeService)
    {
        $this->buildTreeService = $buildTreeService;
    }

    public function getAllProductsForCategory($categoryId)
    {
        $categories = Category::withCount('products')->get()->toArray();
        $categoryIds = [$categoryId];
        $this->buildTreeService->buildTree($categories, $categoryId, $categoryIds);

        return Product::whereIn('cat_id', $categoryIds)
            ->where('visible', 1)->get();
    }
}