<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'parent_id', 'weight'
    ];

    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'cat_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id')->withCount('products');
    }
}
